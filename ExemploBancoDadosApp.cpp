/***************************************************************
 * Name:      ExemploBancoDadosApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2014-11-26
 * Copyright:  ()
 * License:
 **************************************************************/

#include "ExemploBancoDadosApp.h"

//(*AppHeaders
#include "ExemploBancoDadosMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(ExemploBancoDadosApp);

bool ExemploBancoDadosApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	ExemploBancoDadosDialog Dlg(0);
    	SetTopWindow(&Dlg);
    	Dlg.ShowModal();
    	wxsOK = false;
    }
    //*)
    return wxsOK;

}
