/***************************************************************
 * Name:      ExemploBancoDadosMain.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2014-11-26
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef EXEMPLOBANCODADOSMAIN_H
#define EXEMPLOBANCODADOSMAIN_H

//(*Headers(ExemploBancoDadosDialog)
#include <wx/grid.h>
#include <wx/dialog.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class ExemploBancoDadosDialog: public wxDialog
{
    public:

        ExemploBancoDadosDialog(wxWindow* parent,wxWindowID id = -1);
        virtual ~ExemploBancoDadosDialog();

    private:

        //(*Handlers(ExemploBancoDadosDialog)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnbtnConectarClick(wxCommandEvent& event);
        void OnbtnDesconectarClick(wxCommandEvent& event);
        void OnbtnExecRespostaClick(wxCommandEvent& event);
        void OnbtnExecNoRespostaClick(wxCommandEvent& event);
        //*)

        //(*Identifiers(ExemploBancoDadosDialog)
        static const long ID_BUTTON1;
        static const long ID_BUTTON2;
        static const long ID_GRID1;
        static const long ID_TEXTCTRL1;
        static const long ID_BUTTON5;
        static const long ID_BUTTON6;
        static const long ID_STATICTEXT1;
        static const long ID_STATICTEXT2;
        //*)

        //(*Declarations(ExemploBancoDadosDialog)
        wxButton* btnExecResposta;
        wxButton* btnDesconectar;
        wxButton* btnExecNoResposta;
        wxTextCtrl* txtComando;
        wxStaticText* txtResultado;
        wxStaticText* StaticText1;
        wxGrid* tbl;
        wxButton* btnConectar;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // EXEMPLOBANCODADOSMAIN_H
