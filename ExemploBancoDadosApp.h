/***************************************************************
 * Name:      ExemploBancoDadosApp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2014-11-26
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef EXEMPLOBANCODADOSAPP_H
#define EXEMPLOBANCODADOSAPP_H

#include <wx/app.h>

class ExemploBancoDadosApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // EXEMPLOBANCODADOSAPP_H
