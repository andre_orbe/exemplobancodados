#ifndef BANCODADOS_INCLUDED
#define BANCODADOS_INCLUDED
#include <iostream>
#include <mysql++.h>
using namespace std;
using namespace mysqlpp;

class BancoDados
{
    public:
        static BancoDados* instance();
        virtual ~BancoDados();
        bool Conectar(const char *banco, const char* servidor, const char* usuario, const char* senha);
        void Desconectar();
        bool Executar(const char* comando, bool retorna);
        string LerResposta(unsigned int linha, unsigned int coluna);
        int LerValor(unsigned int linha, unsigned int coluna);
        int Linhas();
        int Colunas();

    protected:
    private:
        BancoDados();
        static BancoDados *p;
        Connection *conexao;
        StoreQueryResult resultado;
};

#endif // BANCODADOS_INCLUDED
