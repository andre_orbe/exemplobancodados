/***************************************************************
 * Name:      ExemploBancoDadosMain.cpp
 * Purpose:   Code for Application Frame
 * Author:     ()
 * Created:   2014-11-26
 * Copyright:  ()
 * License:
 **************************************************************/
#include "BancoDados.h"
#include "ExemploBancoDadosMain.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(ExemploBancoDadosDialog)
#include <wx/string.h>
#include <wx/intl.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(ExemploBancoDadosDialog)
const long ExemploBancoDadosDialog::ID_BUTTON1 = wxNewId();
const long ExemploBancoDadosDialog::ID_BUTTON2 = wxNewId();
const long ExemploBancoDadosDialog::ID_GRID1 = wxNewId();
const long ExemploBancoDadosDialog::ID_TEXTCTRL1 = wxNewId();
const long ExemploBancoDadosDialog::ID_BUTTON5 = wxNewId();
const long ExemploBancoDadosDialog::ID_BUTTON6 = wxNewId();
const long ExemploBancoDadosDialog::ID_STATICTEXT1 = wxNewId();
const long ExemploBancoDadosDialog::ID_STATICTEXT2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ExemploBancoDadosDialog,wxDialog)
    //(*EventTable(ExemploBancoDadosDialog)
    //*)
END_EVENT_TABLE()

ExemploBancoDadosDialog::ExemploBancoDadosDialog(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(ExemploBancoDadosDialog)
    Create(parent, id, _("wxWidgets app"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
    SetClientSize(wxSize(743,324));
    btnDesconectar = new wxButton(this, ID_BUTTON1, _("Desconectar"), wxPoint(112,8), wxSize(104,32), 0, wxDefaultValidator, _T("ID_BUTTON1"));
    btnConectar = new wxButton(this, ID_BUTTON2, _("Conectar"), wxPoint(8,8), wxSize(104,32), 0, wxDefaultValidator, _T("ID_BUTTON2"));
    tbl = new wxGrid(this, ID_GRID1, wxPoint(8,96), wxSize(728,144), 0, _T("ID_GRID1"));
    tbl->CreateGrid(2,2);
    tbl->EnableEditing(true);
    tbl->EnableGridLines(true);
    tbl->SetDefaultCellFont( tbl->GetFont() );
    tbl->SetDefaultCellTextColour( tbl->GetForegroundColour() );
    txtComando = new wxTextCtrl(this, ID_TEXTCTRL1, _("SELECT * FROM world.country;"), wxPoint(8,40), wxSize(728,48), wxTE_MULTILINE, wxDefaultValidator, _T("ID_TEXTCTRL1"));
    btnExecResposta = new wxButton(this, ID_BUTTON5, _("Executar c/ Resposta"), wxPoint(216,8), wxSize(145,32), 0, wxDefaultValidator, _T("ID_BUTTON5"));
    btnExecResposta->Disable();
    btnExecNoResposta = new wxButton(this, ID_BUTTON6, _("Executar s/ Resposta"), wxPoint(360,8), wxSize(145,32), 0, wxDefaultValidator, _T("ID_BUTTON6"));
    btnExecNoResposta->Disable();
    txtResultado = new wxStaticText(this, ID_STATICTEXT1, _("Resultado: \?\?"), wxPoint(512,16), wxSize(224,17), 0, _T("ID_STATICTEXT1"));
    StaticText1 = new wxStaticText(this, ID_STATICTEXT2, _("Após conectar ao banco será possível executar comandos sem resposta (INSERT, DELETE, UPDATE) ou com resposta (SELECT). Este último terá seus resultados mostrados na tabela acima. Os comandos devem ser escrito no campo mais acima.\nEste exemplo, para fins didáticos, não emprega algumas recomendações do POO.\n"), wxPoint(8,248), wxSize(728,72), 0, _T("ID_STATICTEXT2"));

    Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ExemploBancoDadosDialog::OnbtnDesconectarClick);
    Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ExemploBancoDadosDialog::OnbtnConectarClick);
    Connect(ID_BUTTON5,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ExemploBancoDadosDialog::OnbtnExecRespostaClick);
    Connect(ID_BUTTON6,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ExemploBancoDadosDialog::OnbtnExecNoRespostaClick);
    //*)
}

ExemploBancoDadosDialog::~ExemploBancoDadosDialog()
{
    //(*Destroy(ExemploBancoDadosDialog)
    //*)
}

void ExemploBancoDadosDialog::OnQuit(wxCommandEvent& event)
{
    Close();
}

void ExemploBancoDadosDialog::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void ExemploBancoDadosDialog::OnbtnConectarClick(wxCommandEvent& event)
{
    if (BancoDados::instance()->Conectar("world","localhost", "root", "root"))
    {
        btnExecResposta->Enable(true);
        btnExecNoResposta->Enable(true);
    }
}

void ExemploBancoDadosDialog::OnbtnDesconectarClick(wxCommandEvent& event)
{
    BancoDados::instance()->Desconectar();
    btnExecResposta->Enable(false);
    btnExecNoResposta->Enable(false);
}

void ExemploBancoDadosDialog::OnbtnExecRespostaClick(wxCommandEvent& event)
{
    wxString comandoSQL = txtComando->GetValue();
    bool resposta = BancoDados::instance()->Executar(comandoSQL.ToAscii(), true);
    if (resposta)
        txtResultado->SetLabel(_("Resultado OK"));
    else
        txtResultado->SetLabel(_("ERRO!"));
    // Mostra o resultado no wxGrid
    long linhas = BancoDados::instance()->Linhas();
    long colunas = BancoDados::instance()->Colunas();

    tbl->DeleteRows(0, tbl->GetNumberRows());   // Remove todas as linhas
    tbl->InsertRows(0, linhas); // Insere a quantidade de linhas necessarias
    tbl->DeleteCols(0,tbl->GetNumberCols());
    tbl->InsertCols(0, colunas - 1);
    // Exemplo para mudança do cabeçalho
    tbl->SetColLabelValue(0, _("País"));
    tbl->SetColLabelValue(1, _("Coluna 1"));

    for (long i=0; i<tbl->GetNumberRows(); i++)
    {
        for (long j=0; j<tbl->GetNumberCols()+1; j++)
        {
            string respMysql = BancoDados::instance()->LerResposta(i, j);
            wxString respWx = wxString::FromUTF8(respMysql.c_str());
            if (j == 0)
            {
                tbl->SetRowLabelValue(i, respWx);
            }
            else
            {
                tbl->SetCellValue(i, j-1, respWx);
            }
        }
    }


}

void ExemploBancoDadosDialog::OnbtnExecNoRespostaClick(wxCommandEvent& event)
{
    wxString comandoSQL = txtComando->GetValue();
    bool resposta = BancoDados::instance()->Executar(comandoSQL.ToAscii(), false);
    if (resposta)
        txtResultado->SetLabel(_("Resultado OK"));
    else
        txtResultado->SetLabel(_("ERRO!"));
}
