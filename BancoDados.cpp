#include "BancoDados.h"

BancoDados *BancoDados::p = NULL;

BancoDados* BancoDados::instance()
{
    if (p == NULL)
    {
        p = new BancoDados();
    }
    return p;
}

bool BancoDados::Conectar(const char *banco, const char* servidor, const char* usuario, const char* senha)
{
    if (conexao->connected())
        return true;
    if (conexao->connect(banco, servidor, usuario, senha))
        return true;
    else
        return false;
}

void BancoDados::Desconectar()
{
    conexao->disconnect();
}

bool BancoDados::Executar(const char* comando, bool retorna)
{
    if ((!conexao->connected()) && (!strlen(comando)==0))
        return false;
    Query consulta = conexao->query();
    consulta << comando;
    if (retorna)
    {
        resultado = consulta.store();
        if (resultado)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        resultado.clear();
        return consulta.execute();
    }
}

int BancoDados::Colunas()
{
    if (resultado)
        return resultado.num_fields();
    else
        return 0;
}

int BancoDados::Linhas()
{
    if (resultado)
        return resultado.num_rows();
    else
        return 0;
}

string BancoDados::LerResposta(unsigned int linha, unsigned int coluna)
{
    if (resultado)
    {
        if ((resultado.num_rows() > linha) && (resultado.num_fields() > coluna))
        {
            string dado;
            resultado[linha][coluna].to_string(dado);
            return dado;
        }
    }
    return "Erro";
}

int BancoDados::LerValor(unsigned int linha, unsigned int coluna)
{
    if (resultado)
    {
        if ((resultado.num_rows() > linha) && (resultado.num_fields() > coluna))
        {
            return atoi(resultado[linha][coluna]);
        }
    }
    return -1;
}

BancoDados::BancoDados()
{
    conexao = new Connection(false);
}

BancoDados::~BancoDados()
{
    delete conexao;
}
